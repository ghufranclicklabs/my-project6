//
//  ViewController.swift
//  Tic Tac Toe
//
//  Created by clicklabs92 on 03/02/15.
//  Copyright (c) 2015 clicklabs92. All rights reserved.
//

import UIKit
     //define globally variable that accesed by other controller
   var playerOneNameGlobal = ""
   var playerTwoNameGlobal = ""
   var playerOneWinScores = 0
   var playerTwoWinScores = 0
   var tieMatches = 0
   var pressSaveButton = 0
   var totalMatchesPlayed = 0
   var playerOneLossScores = 0
   var playerTwoLossScores = 0

  
  //first view controller
     class ViewController: UIViewController {
         var counter = 0
         var goNumber = 1
         var winner = 0
          //0 empty, 1= not1, 2= cross1
        var gameState = [0, 0, 0, 0, 0, 0, 0, 0, 0]
       
        //create immutable object i.e codition for winning the match
        let winningCombinations = [[0,1,2], [3,4,5], [6,7,8], [0,3,6], [1,4,7], [2,5,8], [0,4,8], [2,4,6]]
        
        //label for button  0 to 8 in tic tac toe board
        @IBOutlet weak var button0: UIButton!
       //label field displaying message
        @IBOutlet weak var label: UILabel!
        @IBOutlet weak var playAgain: UIButton!
        //play button Pressed Action
        @IBAction func playButtonPressed(sender: AnyObject) {
               goNumber = 1
                winner = 0
                gameState = [0, 0, 0, 0, 0, 0, 0, 0, 0]
                label.center = CGPointMake(label.center.x - 400, label.center.y)
                playAgain.alpha = 0
                label.hidden = true
                 var button : UIButton
            for var i = 1; i < 10 ; i++ {
                 button = view.viewWithTag(i) as UIButton
                  button.setImage(nil, forState: .Normal)
                totalMatchesPlayed = playerOneWinScores + playerTwoWinScores + tieMatches
            
        }
 }
    //image change when pressing button 0 to 9
    @IBAction func buttonPressed(sender: AnyObject) {
        if (gameState[sender.tag-1]==0 && winner == 0) {
        //print image according codition
        var image = UIImage()
        if (goNumber%2 == 0) {
         image = UIImage(named: "cross1.png")!
            gameState[sender.tag-1] = 2
        } else {
             image = UIImage(named: "not1.png")!
            gameState[sender.tag-1] = 1
        }
           for combination in winningCombinations {
                if (gameState[combination[0]]==gameState[combination[1]] && gameState[combination[1]]==gameState[combination[2]] && gameState[combination[0]] != 0) {
                    winner = gameState[combination[0]]
                   
                }
            }
            
            //after winning label appeared with win message
            if (winner != 0) {
                if (winner == 1) {
                  label.hidden = false
                    label.text = "Congratulation " + playerOneNameGlobal + " win the match"
                    playerOneWinScores++
                } else {
                    label.hidden = false
                    label.text = "Congratulation " + playerTwoNameGlobal + " win the match"
                     playerTwoWinScores++
                }
                //function for label apper in controller from position x and y axis
                UIView.animateWithDuration(0.4, animations: {
                    self.label.center = CGPointMake(self.label.center.x + 400, self.label.center.y)
                    self.playAgain.alpha = 1
                })
            } else if winner == 0 && goNumber == 9 {
                 tieMatches++
                let alert = UIAlertController(title: "Tic Tac Toe", message: "It was a tie", preferredStyle: UIAlertControllerStyle.Alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: { (alert:UIAlertAction!) -> Void in
                    //TODO reset the fields
                    self.goNumber = 1
                    self.winner = 0
                    self.gameState = [0, 0, 0, 0, 0, 0, 0, 0, 0]
                    self.label.center = CGPointMake(self.label.center.x - 400, self.label.center.y)
                    self.playAgain.alpha = 0
                    var button : UIButton
                    for var i = 1; i < 10 ; i++ {
                        button = self.view.viewWithTag(i) as UIButton
                        button.setImage(nil, forState: .Normal)
                    }
                }))
                self.presentViewController(alert, animated: true, completion: nil)
               }
        goNumber++
            sender.setImage(image, forState: .Normal)
        }
    }
    //pressing score button reset the entries and go next controller
       @IBAction func resetButton(sender: AnyObject) {
        self.goNumber = 1
        self.winner = 0
        self.gameState = [0, 0, 0, 0, 0, 0, 0, 0, 0]
        self.label.center = CGPointMake(self.label.center.x - 400, self.label.center.y)
        self.playAgain.alpha = 0
        var button : UIButton
            for var i = 1; i < 10 ; i++
            {
                      button = self.view.viewWithTag(i) as UIButton
                      button.setImage(nil, forState: .Normal)
                }
           label.hidden = true
      }
        override func viewDidLoad() {
        super.viewDidLoad()
             // Do any additional setup after loading the view, typically from a nib
            playerOneWinScores = 0
            playerTwoWinScores = 0
            tieMatches = 0
            totalMatchesPlayed = 0
            playerOneLossScores = 0
            playerTwoLossScores = 0
            pressSaveButton = 0
            
        }
        
        
        override func didReceiveMemoryWarning() {
          super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
  override func viewDidAppear(animated: Bool) {
        label.center = CGPointMake(label.center.x - 400, label.center.y)
    self.playAgain.alpha = 0
    }
}

