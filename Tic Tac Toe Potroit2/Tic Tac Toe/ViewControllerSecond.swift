//
//  ViewControllerSecond.swift
//  Tic Tac Toe
//
//  Created by clicklabs92 on 04/02/15.
//  Copyright (c) 2015 clicklabs92. All rights reserved.
//

import UIKit

class ViewControllerSecond: UIViewController {
    
    //label for counting win, loss, total match and tie matches
    @IBOutlet weak var playerOneWinScore: UILabel!
    @IBOutlet weak var playerTwoWinScore: UILabel!
    @IBOutlet weak var tieScores: UILabel!
    @IBOutlet weak var totalMatchesPlay: UILabel!
    @IBOutlet weak var playerOneNameLabel: UILabel!
    @IBOutlet weak var playerTwoNameLabel: UILabel!
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var playerOneLossScore: UILabel!
    @IBOutlet weak var playerTwoLossScore: UILabel!
    
   
    //for printing score when score button tapped
    @IBAction func scoreButton(sender: AnyObject) {
        if totalMatchesPlay != 0 {
          playerOneWinScore.text = "\( playerOneWinScores)"
          playerTwoWinScore.text = "\(playerTwoWinScores)"
          totalMatchesPlayed = playerOneWinScores + playerTwoWinScores + tieMatches
          totalMatchesPlay.text = "\(totalMatchesPlayed)"
          tieScores.text = "\(tieMatches)"
          playerOneLossScore.text  = "\(playerTwoWinScores)"
          playerTwoLossScore.text  = "\(playerOneWinScores)"
          playerOneNameLabel.text = playerOneNameGlobal
          playerTwoNameLabel.text = playerTwoNameGlobal

            //stored item in table permanently
            let fixedtoDoItems = record
            NSUserDefaults.standardUserDefaults().setObject(fixedtoDoItems, forKey: "record")
                NSUserDefaults.standardUserDefaults().synchronize()
           
            //hiding the keyboard
            self.view.endEditing(true)
        }
        /*else {
            //if no match played show the alert...
            let title = "No Match Played!"
            let message = "Please Play The Match..."
            let okText = "Ok"
            let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.Alert)
            let okButton = UIAlertAction(title: okText, style: UIAlertActionStyle.Cancel, handler: nil)
            alert.addAction(okButton)
            presentViewController(alert, animated: true, completion: nil)
            }*/

    }
    @IBAction func saveButton(sender: AnyObject) {
        if totalMatchesPlayed != 0 && pressSaveButton == 0 {
            record.append("             Game : \(totalMatchesPlayed)   \nPlayers :         \(playerOneNameGlobal)  &  \(playerTwoNameGlobal)  \n \(playerOneNameGlobal) Win :      \(playerOneWinScores) \n \(playerTwoNameGlobal) Win :      \(playerTwoWinScores) \n Ties :               \(tieMatches )")
            //these three lines are used to store the items in table permanently...
            let fixedtoDoItem = record
            NSUserDefaults.standardUserDefaults().setObject(fixedtoDoItem, forKey: "record")
            NSUserDefaults.standardUserDefaults().synchronize()
           pressSaveButton++
            
        } else{
            //if no match played show the alert...
            let title = "No Match Played!"
            let message = "Please Play The Match..."
            let okText = "Ok"
            let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.Alert)
            let okButton = UIAlertAction(title: okText, style: UIAlertActionStyle.Cancel, handler: nil)
            alert.addAction(okButton)
            presentViewController(alert, animated: true, completion: nil)
        }
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
      // Do any additional setup after loading the view.
    }
    
    
   override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

